<?php
	if(!empty($_FILES['uploaded_file'])) {
	    $path = $_SERVER["DOCUMENT_ROOT"]."/";
	    $path = $path . basename( $_FILES['uploaded_file']['name']);
		$fileType = strtolower(pathinfo($path,PATHINFO_EXTENSION));
		if($fileType == "") {
			if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $path)) {
				echo "<SCRIPT>
						alert('Verification File Uploaded. Click OK to redirect to Home')
						window.location.replace('index.php');
					</SCRIPT>";
			} else {
				echo "<SCRIPT>
						alert('Verification File Uploaded Failed!. Click OK to Retry')
						window.location.replace('upload_2.html');
					</SCRIPT>";
			}
		} else {
			echo "<SCRIPT>
						alert('Only verification files are allowed!! File Uploaded Failed!. Click OK to Retry')
						window.location.replace('upload_2.html');
					</SCRIPT>";
		}
	}
?>
